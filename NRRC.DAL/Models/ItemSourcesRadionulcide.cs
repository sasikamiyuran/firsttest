﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ItemSourcesRadionulcide
{
    public int Id { get; set; }

    public int ItemSourceProfileId { get; set; }

    public int RadionulcideId { get; set; }

    public string? InitialActivity { get; set; }

    public string? InitialActivityUnit { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ItemSourcesProfile ItemSourceProfile { get; set; } = null!;

    public virtual Radionuclide Radionulcide { get; set; } = null!;
}
