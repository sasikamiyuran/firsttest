﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class NuclearRelatedItemsProfile
{
    public int NrrcrelatedItemId { get; set; }

    public string? RelatedItemDescAr { get; set; }

    public string? RelatedItemDescEn { get; set; }

    public string? NrrcId { get; set; }

    public string? Status { get; set; }

    public string? ManufacturerSerialNo { get; set; }

    public DateTime DateofManufacturing { get; set; }

    public string? FacilityRelatedItemId { get; set; }

    public string? Purpose { get; set; }

    public string? ItemTypeNo { get; set; }

    public string? ItemtypeName { get; set; }

    public string? ModelNumber { get; set; }

    public string? Hscode { get; set; }

    public string? GovernmentCommitmentsFlag { get; set; }

    public string? EndUserCertificateFlag { get; set; }

    public string? Unit { get; set; }

    public int PermitInitialQty { get; set; }

    public int? EntityId { get; set; }

    public int? FacilityId { get; set; }

    public int? LicenseId { get; set; }

    public int? LicenseInventoryId { get; set; }

    public int? PermitdetailsId { get; set; }

    public int? PermitInventoryId { get; set; }

    public int? PractiseId { get; set; }

    public int? Sroid { get; set; }

    public int? LegalRepresentativesId { get; set; }

    public int? ManufacturerId { get; set; }

    public int? ManufactureCountryId { get; set; }

    public int? ItemCategory { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual EntityProfile? Entity { get; set; }

    public virtual FacilityProfile? Facility { get; set; }

    public virtual LookupSet? ItemCategoryNavigation { get; set; }

    public virtual LegalRepresentativesProfile? LegalRepresentatives { get; set; }

    public virtual LicenseProfile? License { get; set; }

    public virtual LicenseInventoryLimit? LicenseInventory { get; set; }

    public virtual BasCountry? ManufactureCountry { get; set; }

    public virtual ManufacturerMaster? Manufacturer { get; set; }

    public virtual PermitInventoryLimit? PermitInventory { get; set; }

    public virtual PermitDetailsProfile? Permitdetails { get; set; }

    public virtual PractiseProfile? Practise { get; set; }

    public virtual SafetyResponsibleOfficersProfile? Sro { get; set; }
}
