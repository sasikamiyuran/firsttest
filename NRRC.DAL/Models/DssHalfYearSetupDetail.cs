﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssHalfYearSetupDetail
{
    public string CustomerId { get; set; } = null!;

    public string CmpNum { get; set; } = null!;

    public string HalfYearCode { get; set; } = null!;

    public string PeriodNum { get; set; } = null!;
}
