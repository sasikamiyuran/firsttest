﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class Screen
{
    public int ScreenId { get; set; }

    public string? ScreenNameAr { get; set; }

    public string? ScreenNameEn { get; set; }

    public string? ScreenCode { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<InternalScreenRole> InternalScreenRoles { get; } = new List<InternalScreenRole>();

    public virtual ICollection<ScreenField> ScreenFields { get; } = new List<ScreenField>();
}
