﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GE = NRRC.Entity;

namespace NRRC.BLL.IService
{
    public interface IPageStructureService
    {
        GE::PageStructure GetPageStructure(string customerId, string companyNumber, string pageId);
    }
}
