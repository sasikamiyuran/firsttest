﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class LicenseInventoryLimit
{
    public int LicenseInventoryId { get; set; }

    public string? SourceType { get; set; }

    public string? Radionuclide { get; set; }

    public int MaximumRadioactivity { get; set; }

    public int NumberofSources { get; set; }

    public int? LicenseId { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual LicenseProfile? License { get; set; }

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
