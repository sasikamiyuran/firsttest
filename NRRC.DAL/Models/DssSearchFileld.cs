﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssSearchFileld
{
    public string CustomerId { get; set; } = null!;

    public string CmpNum { get; set; } = null!;

    public string SearchField { get; set; } = null!;

    public string SearchFieldDescNtv { get; set; } = null!;

    public string SearchFieldDescFrn { get; set; } = null!;

    public string SqlStmt { get; set; } = null!;

    public string? SearchFieldParam { get; set; }

    public double? FieldOrder { get; set; }

    public string? IconFile { get; set; }

    public string? TitleDescFrn { get; set; }

    public string? TitleDescNtv { get; set; }
}
