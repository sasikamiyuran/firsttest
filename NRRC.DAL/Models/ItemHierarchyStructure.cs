﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ItemHierarchyStructure
{
    public string ItemStructureCode { get; set; } = null!;

    public string? ItemStructureDesFrn { get; set; }

    public string? ItemStructureDesNtv { get; set; }

    public bool StructureGeneralDetailFlag { get; set; }

    public int StructureLevelNum { get; set; }

    public string? ParentStructure { get; set; }

    public string? DefaultIssueAccountCode { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<ServiceItemProfile> ServiceItemProfiles { get; } = new List<ServiceItemProfile>();
}
