﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class InternalRole
{
    public int RoleId { get; set; }

    public string? RoleNameAr { get; set; }

    public string? RoleNameEn { get; set; }

    public string? RoleCode { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<InternalScreenRole> InternalScreenRoles { get; } = new List<InternalScreenRole>();
}
