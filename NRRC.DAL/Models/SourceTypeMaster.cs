﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class SourceTypeMaster
{
    public int ItemTypeId { get; set; }

    public string? ItemTypeDescFrn { get; set; }

    public string? ItemTypeDescNtv { get; set; }

    public string? ItemTypeClass { get; set; }

    public string? CreatedUser { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? ModifiedUser { get; set; }

    public DateTime? ModifiedDate { get; set; }
}
