﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class PermitDetailsProfile
{
    public int PermitDetailsId { get; set; }

    public string? PermitDetailsDescAr { get; set; }

    public string? PermitDetailsDescEn { get; set; }

    public string? PermitNumber { get; set; }

    public DateTime EffectiveDate { get; set; }

    public DateTime ExpirationDate { get; set; }

    public int? LicenseId { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual LicenseProfile? License { get; set; }

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<PermitInventoryLimit> PermitInventoryLimits { get; } = new List<PermitInventoryLimit>();

    public virtual ICollection<PractiseProfile> PractiseProfiles { get; } = new List<PractiseProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
