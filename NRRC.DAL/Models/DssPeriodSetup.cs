﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssPeriodSetup
{
    public string CmpNum { get; set; } = null!;

    public string PeriodNum { get; set; } = null!;

    public string? PeriodDescFrn { get; set; }

    public string? PeriodDescNtv { get; set; }

    public string? PeriodShortDesFrn { get; set; }

    public string? PeriodShortDesNtv { get; set; }
}
