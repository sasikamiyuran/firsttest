﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ScreenField
{
    public int FieldId { get; set; }

    public string? FieldDescAr { get; set; }

    public string? FieldDescEn { get; set; }

    public int FieldType { get; set; }

    public string? FieldFormat { get; set; }

    public int ScreenId { get; set; }

    public int? LookupSetId { get; set; }

    public int? LovId { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual LookupSet? LookupSet { get; set; }

    public virtual ListOfValue? Lov { get; set; }

    public virtual Screen Screen { get; set; } = null!;
}
