﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class PermitInventoryLimit
{
    public int PermitInventoryId { get; set; }

    public string? SourceSerialNo { get; set; }

    public string? ManufactureName { get; set; }

    public string? Radionuclide { get; set; }

    public string? ModelMaximumRadioactivity { get; set; }

    public string? Unit { get; set; }

    public int? PermitDetailsId { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual PermitDetailsProfile? PermitDetails { get; set; }

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
