﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class LegalRepresentativesProfile
{
    public int LegalRepresentativesId { get; set; }

    public string? LegalRepresentativesNameAr { get; set; }

    public string? LegalRepresentativesNameEn { get; set; }

    public string? Title { get; set; }

    public string? NationalId { get; set; }

    public string? PhoneNo { get; set; }

    public string? MobileNo { get; set; }

    public string? EmailId { get; set; }

    public string? Status { get; set; }

    public int CurrentFacilities { get; set; }

    public string? Note { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
