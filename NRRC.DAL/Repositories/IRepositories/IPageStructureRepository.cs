﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GE = NRRC.Entity;

namespace NRRC.DAL.Repositories.IRepositories
{
    public interface IPageStructureRepository
    {
        GE::PageStructure GetPageStructures(string customerId, string companyNumber, string pageId);
    }
}
