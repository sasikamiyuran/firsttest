﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class InternalScreenRole
{
    public int ScreenRoleId { get; set; }

    public int ScreenOrder { get; set; }

    public bool Insert { get; set; }

    public bool Update { get; set; }

    public bool Delete { get; set; }

    public bool Query { get; set; }

    public int ScreenId { get; set; }

    public int RoleId { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual InternalRole Role { get; set; } = null!;

    public virtual Screen Screen { get; set; } = null!;
}
