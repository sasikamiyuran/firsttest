﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ListOfValue
{
    public int LovId { get; set; }

    public string? LovNameAr { get; set; }

    public string? LovNameEn { get; set; }

    public string? LovCode { get; set; }

    public string? SqlStatement { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<ScreenField> ScreenFields { get; } = new List<ScreenField>();
}
