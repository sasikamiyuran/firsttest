﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class AspNetUser
{
    public string? FirstNameAr { get; set; }

    public string? SecondNameAr { get; set; }

    public string? LastNameNameAr { get; set; }

    public string? FirstNameEn { get; set; }

    public string? SecondNameEn { get; set; }

    public string? LastNameNameEn { get; set; }

    public bool IsActiveUser { get; set; }

    public int UserTypeId { get; set; }

    public string? PictureContentType { get; set; }

    public string? PictureName { get; set; }

    public string? PassportNo { get; set; }

    public DateTime? GregorianBirthDate { get; set; }

    public DateTime? HijriBirthDate { get; set; }

    public string? NationalId { get; set; }

    public string? IqamaId { get; set; }

    public string? UserAlternatePhone { get; set; }

    public int? Nationality { get; set; }

    public string Id { get; set; } = null!;

    public byte[]? Picture { get; set; }

    public int? CountryId { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime CreatedOn { get; set; }

    public DateTime ModifiedOn { get; set; }

    public string? UserName { get; set; }

    public string? NormalizedUserName { get; set; }

    public string? Email { get; set; }

    public string? NormalizedEmail { get; set; }

    public bool EmailConfirmed { get; set; }

    public string? PasswordHash { get; set; }

    public string? SecurityStamp { get; set; }

    public string? ConcurrencyStamp { get; set; }

    public string? PhoneNumber { get; set; }

    public bool PhoneNumberConfirmed { get; set; }

    public bool TwoFactorEnabled { get; set; }

    public DateTimeOffset? LockoutEnd { get; set; }

    public bool LockoutEnabled { get; set; }

    public int AccessFailedCount { get; set; }

    public virtual ICollection<AspNetUserClaim> AspNetUserClaims { get; } = new List<AspNetUserClaim>();

    public virtual ICollection<AspNetUserLogin> AspNetUserLogins { get; } = new List<AspNetUserLogin>();

    public virtual ICollection<AspNetUserToken> AspNetUserTokens { get; } = new List<AspNetUserToken>();

    public virtual BasCountry? Country { get; set; }

    public virtual ICollection<AspNetRole> Roles { get; } = new List<AspNetRole>();
}
