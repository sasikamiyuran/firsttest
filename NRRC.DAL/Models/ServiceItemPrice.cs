﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ServiceItemPrice
{
    public int ServiceItemPriceId { get; set; }

    public int ServiceItemId { get; set; }

    public int LineNum { get; set; }

    public decimal ItemPrice { get; set; }

    public bool ActiveFlag { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ServiceItemProfile ServiceItem { get; set; } = null!;
}
