﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class LicenseProfile
{
    public int LicenseId { get; set; }

    public string? LicenseDescAr { get; set; }

    public string? LicenseDescEn { get; set; }

    public string? LicenseCode { get; set; }

    public string? LicenseVersionNumber { get; set; }

    public DateTime EffectiveDate { get; set; }

    public DateTime ExpirationDate { get; set; }

    public string? PractiseSector { get; set; }

    public string? LicenseActivities { get; set; }

    public int? EntityId { get; set; }

    public int? FacilityId { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public virtual EntityProfile? Entity { get; set; }

    public virtual FacilityProfile? Facility { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<LicenseInventoryLimit> LicenseInventoryLimits { get; } = new List<LicenseInventoryLimit>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<PermitDetailsProfile> PermitDetailsProfiles { get; } = new List<PermitDetailsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
