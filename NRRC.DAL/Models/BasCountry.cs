﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class BasCountry
{
    public int CountryId { get; set; }

    public string CountryNameAr { get; set; } = null!;

    public string CountryNameEn { get; set; } = null!;

    public string NationalityNameFrn { get; set; } = null!;

    public string NationalityNameNtv { get; set; } = null!;

    public string CountryCodeIso { get; set; } = null!;

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<AspNetUser> AspNetUsers { get; } = new List<AspNetUser>();

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<ManufacturerMaster> ManufacturerMasters { get; } = new List<ManufacturerMaster>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();
}
