﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssQuarterSetup
{
    public string CustomerId { get; set; } = null!;

    public string CmpNum { get; set; } = null!;

    public string QuarterCode { get; set; } = null!;

    public string? QuarterDescFrn { get; set; }

    public string? QuarterDescNtv { get; set; }
}
