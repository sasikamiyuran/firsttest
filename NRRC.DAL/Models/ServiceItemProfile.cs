﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ServiceItemProfile
{
    public int ServiceItemId { get; set; }

    public string ItemDesFrn { get; set; } = null!;

    public string ItemDesNtv { get; set; } = null!;

    public string ItemStructureCode { get; set; } = null!;

    public string? ItemStructureCode1 { get; set; }

    public decimal CurrentPrice { get; set; }

    public string? IssueAccountCode { get; set; }

    public string? ItemType { get; set; }

    public string? ItemRefCode { get; set; }

    public bool ActiveFlag { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ItemHierarchyStructure? ItemStructureCode1Navigation { get; set; }

    public virtual ICollection<ServiceItemPrice> ServiceItemPrices { get; } = new List<ServiceItemPrice>();
}
