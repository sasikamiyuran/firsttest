﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class BasCity
{
    public int CityId { get; set; }

    public int CountryId { get; set; }

    public string NameAr { get; set; } = null!;

    public string NameEn { get; set; } = null!;

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public string? CityAbbrCode { get; set; }
}
