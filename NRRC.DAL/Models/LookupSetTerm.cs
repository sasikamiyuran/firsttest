﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class LookupSetTerm
{
    public int LookupSetTermId { get; set; }

    public int LookupSetId { get; set; }

    public string? Value { get; set; }

    public string? DisplayNameAr { get; set; }

    public string? DisplayNameEn { get; set; }

    public string? ExtraData1 { get; set; }

    public string? ExtraData2 { get; set; }

    public bool IsActive { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual LookupSet LookupSet { get; set; } = null!;
}
