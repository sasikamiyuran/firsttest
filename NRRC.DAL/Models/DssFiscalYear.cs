﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssFiscalYear
{
    public string? FiscalYear { get; set; }
}
