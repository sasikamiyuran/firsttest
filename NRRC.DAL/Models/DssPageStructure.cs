﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssPageStructure
{
    public string CustomerId { get; set; } = null!;

    public string CmpNum { get; set; } = null!;

    public string PageId { get; set; } = null!;

    public string? PageDescFrn { get; set; }

    public string? PageDescNtv { get; set; }

    public string? PageDataTable { get; set; }

    public string? Seg1 { get; set; }

    public string? Seg1LabelFrn { get; set; }

    public string? Seg1LabelNtv { get; set; }

    public string? Seg2 { get; set; }

    public string? Seg2LabelFrn { get; set; }

    public string? Seg2LabelNtv { get; set; }

    public string? Seg3 { get; set; }

    public string? Seg3LabelFrn { get; set; }

    public string? Seg3LabelNtv { get; set; }

    public string? Seg4 { get; set; }

    public string? Seg4LabelFrn { get; set; }

    public string? Seg4LabelNtv { get; set; }

    public string? Seg5 { get; set; }

    public string? Seg5LabelFrn { get; set; }

    public string? Seg5LabelNtv { get; set; }

    public string? Seg6 { get; set; }

    public string? Seg6LabelFrn { get; set; }

    public string? Seg6LabelNtv { get; set; }

    public string? Seg7 { get; set; }

    public string? Seg7LabelFrn { get; set; }

    public string? Seg7LabelNtv { get; set; }

    public string? Seg8 { get; set; }

    public string? Seg8LabelFrn { get; set; }

    public string? Seg8LabelNtv { get; set; }

    public string? Seg9 { get; set; }

    public string? Seg9LabelFrn { get; set; }

    public string? Seg9LabelNtv { get; set; }

    public string? Seg10 { get; set; }

    public string? Seg10LabelFrn { get; set; }

    public string? Seg10LabelNtv { get; set; }
}
