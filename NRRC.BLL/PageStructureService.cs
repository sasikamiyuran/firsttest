﻿using NRRC.BLL.IService;
using NRRC.Entity;

namespace NRRC.BLL
{
    public class PageStructureService : IPageStructureService
    {
        private readonly IPageStructureService _pageStructure;
        public PageStructureService(IPageStructureService pageStructure)
        {
            _pageStructure = pageStructure;
        }

        public PageStructure GetPageStructure(string customerId, string companyNumber, string pageId)
        {
            return _pageStructure.GetPageStructure(customerId, companyNumber, pageId);
        }
    }
}