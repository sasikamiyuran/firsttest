﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ManufacturerMaster
{
    public int ManufacturerId { get; set; }

    public string? ManufacturerDescAr { get; set; }

    public string? ManufacturerDescEn { get; set; }

    public string? PhoneNo { get; set; }

    public string? MobileNo { get; set; }

    public string? EmailId { get; set; }

    public string? Location { get; set; }

    public string? AddressLine1 { get; set; }

    public string? AddressLine2 { get; set; }

    public string? AddressLine3 { get; set; }

    public int City { get; set; }

    public string? ZipCode { get; set; }

    public string? Pobox { get; set; }

    public int? CountryId { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual BasCountry? Country { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
