﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssQuarterSetupDetail
{
    public string CustomerId { get; set; } = null!;

    public string CmpNum { get; set; } = null!;

    public string QuarterCode { get; set; } = null!;

    public string PeriodNum { get; set; } = null!;
}
