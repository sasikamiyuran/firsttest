﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class EntityProfile
{
    public int EntityId { get; set; }

    public string? EntityNameAr { get; set; }

    public string? EntityNameEn { get; set; }

    public string? PhoneNo { get; set; }

    public string? MobileNo { get; set; }

    public string? EmailId { get; set; }

    public string? GovernmentId { get; set; }

    public string? EntityType { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<FacilityProfile> FacilityProfiles { get; } = new List<FacilityProfile>();

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<LicenseProfile> LicenseProfiles { get; } = new List<LicenseProfile>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
