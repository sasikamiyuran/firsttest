﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ItemSourcesProfile
{
    public int SourceId { get; set; }

    public string? SourceDescAr { get; set; }

    public string? SourceDescEn { get; set; }

    public string? NrrcId { get; set; }

    public string? Status { get; set; }

    public string? ManufacturerSerialNo { get; set; }

    public string? FacilitySerialNo { get; set; }

    public string? RadioNuclides { get; set; }

    public DateTime ProductionDate { get; set; }

    public int IsoptopHalfLifeTime { get; set; }

    public string? IspotopHalfLifeTimeUnit { get; set; }

    public string? InitialActivity { get; set; }

    public string? InitialActivityUnit { get; set; }

    public string? CurrentActivity { get; set; }

    public string? CurrentActivityUnit { get; set; }

    public string? PhysicalForm { get; set; }

    public int Qty { get; set; }

    public string? Unit { get; set; }

    public string? Exemption { get; set; }

    public int RecommendedWorkingLifeTime { get; set; }

    public string? RadioactiveSourceid { get; set; }

    public string? ShieldMaterial { get; set; }

    public string? ShieldNuclearMaterialCode { get; set; }

    public string? Purpose { get; set; }

    public int NumberOfAcquiredSources { get; set; }

    public int NumberOfConsumedSources { get; set; }

    public int NumberOfAvailableSources { get; set; }

    public string? NuclearMaterial { get; set; }

    public int WeightOfNuclearMaterial { get; set; }

    public string? EnrichmentofU235 { get; set; }

    public string? WeightOffIssileisotopes { get; set; }

    public string? WeightOffIssileisotopesunit { get; set; }

    public string? ChemicalForm { get; set; }

    public string? ChemicalComposition { get; set; }

    public int ContainerVolume { get; set; }

    public string? SourceActivity { get; set; }

    public string? SourceModel { get; set; }

    public string? AssociatedEquipment { get; set; }

    public string? SourceCategory { get; set; }

    public int SourceLinkedFlag { get; set; }

    public int? EntityId { get; set; }

    public int? FacilityId { get; set; }

    public int? LicenseId { get; set; }

    public int? LicenseInventoryId { get; set; }

    public int? PermitdetailsId { get; set; }

    public int? PermitInventoryId { get; set; }

    public int? PractiseId { get; set; }

    public int? Sroid { get; set; }

    public int? LegalRepresentativesId { get; set; }

    public int? ManufacturerId { get; set; }

    public int? ManufactureCountryId { get; set; }

    public int? ItemTypeId { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual EntityProfile? Entity { get; set; }

    public virtual FacilityProfile? Facility { get; set; }

    public virtual ICollection<ItemSourcesRadionulcide> ItemSourcesRadionulcides { get; } = new List<ItemSourcesRadionulcide>();

    public virtual LookupSet? ItemType { get; set; }

    public virtual LegalRepresentativesProfile? LegalRepresentatives { get; set; }

    public virtual LicenseProfile? License { get; set; }

    public virtual LicenseInventoryLimit? LicenseInventory { get; set; }

    public virtual BasCountry? ManufactureCountry { get; set; }

    public virtual ManufacturerMaster? Manufacturer { get; set; }

    public virtual PermitInventoryLimit? PermitInventory { get; set; }

    public virtual PermitDetailsProfile? Permitdetails { get; set; }

    public virtual PractiseProfile? Practise { get; set; }

    public virtual SafetyResponsibleOfficersProfile? Sro { get; set; }
}
