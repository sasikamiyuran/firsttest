﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class FacilityProfile
{
    public int FacilityId { get; set; }

    public string? FacilityNameAr { get; set; }

    public string? FacilityNameEn { get; set; }

    public string? OragnizationName { get; set; }

    public string? FacilityCode { get; set; }

    public string? Province { get; set; }

    public string? City { get; set; }

    public string? Location { get; set; }

    public int? EntityId { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual EntityProfile? Entity { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<LicenseProfile> LicenseProfiles { get; } = new List<LicenseProfile>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();

    public virtual ICollection<Worker> Workers { get; } = new List<Worker>();
}
