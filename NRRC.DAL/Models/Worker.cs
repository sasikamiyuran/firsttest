﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class Worker
{
    public int Id { get; set; }

    public string? WorkerNameAr { get; set; }

    public string? WorkerNameEn { get; set; }

    public DateTime BirthDate { get; set; }

    public string? JobPosition { get; set; }

    public int? FacilityId { get; set; }

    public string? NationalityId { get; set; }

    public int? Status { get; set; }

    public int? LookupSetId { get; set; }

    public string? PassportNo { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual FacilityProfile? Facility { get; set; }

    public virtual LookupSet? LookupSet { get; set; }
}
