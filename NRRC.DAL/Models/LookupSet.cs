﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class LookupSet
{
    public int LookupSetId { get; set; }

    public int ParentId { get; set; }

    public string? ClassName { get; set; }

    public string? DisplayNameAr { get; set; }

    public string? DisplayNameEn { get; set; }

    public string? ExtraData1 { get; set; }

    public string? ExtraData2 { get; set; }

    public bool IsActive { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<LookupSetTerm> LookupSetTerms { get; } = new List<LookupSetTerm>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();

    public virtual ICollection<ScreenField> ScreenFields { get; } = new List<ScreenField>();

    public virtual ICollection<Worker> Workers { get; } = new List<Worker>();
}
