﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class TreeControl
{
    public string TreeCode { get; set; } = null!;

    public int LevelNum { get; set; }

    public int? LevelInterval { get; set; }

    public int? LevelLength { get; set; }

    public string? LevelPadding { get; set; }

    public string? LevelTitleFrn { get; set; }

    public string? LevelTitleNtv { get; set; }

    public int? StartingNum { get; set; }
}
