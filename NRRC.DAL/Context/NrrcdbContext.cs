﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using NRRC.DAL.Models;

namespace NRRC.DAL.Context;

public partial class NrrcdbContext : DbContext
{
    public NrrcdbContext()
    {
    }

    public NrrcdbContext(DbContextOptions<NrrcdbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<AspNetRole> AspNetRoles { get; set; }

    public virtual DbSet<AspNetRoleClaim> AspNetRoleClaims { get; set; }

    public virtual DbSet<AspNetUser> AspNetUsers { get; set; }

    public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }

    public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }

    public virtual DbSet<AspNetUserToken> AspNetUserTokens { get; set; }

    public virtual DbSet<BasCity> BasCities { get; set; }

    public virtual DbSet<BasCountry> BasCountries { get; set; }

    public virtual DbSet<DssFiscalYear> DssFiscalYears { get; set; }

    public virtual DbSet<DssHalfYearSetup> DssHalfYearSetups { get; set; }

    public virtual DbSet<DssHalfYearSetupDetail> DssHalfYearSetupDetails { get; set; }

    public virtual DbSet<DssPageStructure> DssPageStructures { get; set; }

    public virtual DbSet<DssPeriodSetup> DssPeriodSetups { get; set; }

    public virtual DbSet<DssQuarterSetup> DssQuarterSetups { get; set; }

    public virtual DbSet<DssQuarterSetupDetail> DssQuarterSetupDetails { get; set; }

    public virtual DbSet<DssSearchFileld> DssSearchFilelds { get; set; }

    public virtual DbSet<DssSearchFileldsOption> DssSearchFileldsOptions { get; set; }

    public virtual DbSet<EntityProfile> EntityProfiles { get; set; }

    public virtual DbSet<FacilityProfile> FacilityProfiles { get; set; }

    public virtual DbSet<InternalRole> InternalRoles { get; set; }

    public virtual DbSet<InternalScreenRole> InternalScreenRoles { get; set; }

    public virtual DbSet<ItemHierarchyStructure> ItemHierarchyStructures { get; set; }

    public virtual DbSet<ItemSourcesFile> ItemSourcesFiles { get; set; }

    public virtual DbSet<ItemSourcesProfile> ItemSourcesProfiles { get; set; }

    public virtual DbSet<ItemSourcesRadionulcide> ItemSourcesRadionulcides { get; set; }

    public virtual DbSet<LegalRepresentativesProfile> LegalRepresentativesProfiles { get; set; }

    public virtual DbSet<LicenseInventoryLimit> LicenseInventoryLimits { get; set; }

    public virtual DbSet<LicenseProfile> LicenseProfiles { get; set; }

    public virtual DbSet<ListOfValue> ListOfValues { get; set; }

    public virtual DbSet<LookupSet> LookupSets { get; set; }

    public virtual DbSet<LookupSetTerm> LookupSetTerms { get; set; }

    public virtual DbSet<ManufacturerMaster> ManufacturerMasters { get; set; }

    public virtual DbSet<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; set; }

    public virtual DbSet<PermitDetailsProfile> PermitDetailsProfiles { get; set; }

    public virtual DbSet<PermitInventoryLimit> PermitInventoryLimits { get; set; }

    public virtual DbSet<PractiseProfile> PractiseProfiles { get; set; }

    public virtual DbSet<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; set; }

    public virtual DbSet<Radionuclide> Radionuclides { get; set; }

    public virtual DbSet<SafetyResponsibleOfficersProfile> SafetyResponsibleOfficersProfiles { get; set; }

    public virtual DbSet<Screen> Screens { get; set; }

    public virtual DbSet<ScreenField> ScreenFields { get; set; }

    public virtual DbSet<ServiceItemPrice> ServiceItemPrices { get; set; }

    public virtual DbSet<ServiceItemProfile> ServiceItemProfiles { get; set; }

    public virtual DbSet<SourceTypeMaster> SourceTypeMasters { get; set; }

    public virtual DbSet<TreeControl> TreeControls { get; set; }

    public virtual DbSet<Worker> Workers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=216.48.186.105,1466;User Id=nirs;Password=Nirs@00_11; Database=NRRCDb; Trusted_Connection=False; Encrypt=False;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AspNetRole>(entity =>
        {
            entity.Property(e => e.Name).HasMaxLength(256);
            entity.Property(e => e.NormalizedName).HasMaxLength(256);
        });

        modelBuilder.Entity<AspNetRoleClaim>(entity =>
        {
            entity.Property(e => e.RoleId).HasMaxLength(450);

            entity.HasOne(d => d.Role).WithMany(p => p.AspNetRoleClaims).HasForeignKey(d => d.RoleId);
        });

        modelBuilder.Entity<AspNetUser>(entity =>
        {
            entity.Property(e => e.CountryId).HasColumnName("CountryID");
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.Email).HasMaxLength(256);
            entity.Property(e => e.FirstNameAr).HasMaxLength(200);
            entity.Property(e => e.FirstNameEn).HasMaxLength(200);
            entity.Property(e => e.IqamaId)
                .HasMaxLength(15)
                .HasColumnName("IqamaID");
            entity.Property(e => e.LastNameNameAr).HasMaxLength(200);
            entity.Property(e => e.LastNameNameEn).HasMaxLength(200);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NationalId)
                .HasMaxLength(15)
                .HasColumnName("NationalID");
            entity.Property(e => e.NormalizedEmail).HasMaxLength(256);
            entity.Property(e => e.NormalizedUserName).HasMaxLength(256);
            entity.Property(e => e.PassportNo).HasMaxLength(15);
            entity.Property(e => e.SecondNameAr).HasMaxLength(200);
            entity.Property(e => e.SecondNameEn).HasMaxLength(200);
            entity.Property(e => e.UserAlternatePhone).HasMaxLength(15);
            entity.Property(e => e.UserName).HasMaxLength(256);

            entity.HasOne(d => d.Country).WithMany(p => p.AspNetUsers).HasForeignKey(d => d.CountryId);

            entity.HasMany(d => d.Roles).WithMany(p => p.Users)
                .UsingEntity<Dictionary<string, object>>(
                    "AspNetUserRole",
                    r => r.HasOne<AspNetRole>().WithMany().HasForeignKey("RoleId"),
                    l => l.HasOne<AspNetUser>().WithMany().HasForeignKey("UserId"),
                    j =>
                    {
                        j.HasKey("UserId", "RoleId");
                    });
        });

        modelBuilder.Entity<AspNetUserClaim>(entity =>
        {
            entity.Property(e => e.UserId).HasMaxLength(450);

            entity.HasOne(d => d.User).WithMany(p => p.AspNetUserClaims).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<AspNetUserLogin>(entity =>
        {
            entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

            entity.Property(e => e.UserId).HasMaxLength(450);

            entity.HasOne(d => d.User).WithMany(p => p.AspNetUserLogins).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<AspNetUserToken>(entity =>
        {
            entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

            entity.HasOne(d => d.User).WithMany(p => p.AspNetUserTokens).HasForeignKey(d => d.UserId);
        });

        modelBuilder.Entity<BasCity>(entity =>
        {
            entity.HasKey(e => new { e.CountryId, e.CityId });

            entity.Property(e => e.CityId).ValueGeneratedOnAdd();
            entity.Property(e => e.CityAbbrCode).HasMaxLength(7);
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NameAr).HasMaxLength(200);
            entity.Property(e => e.NameEn).HasMaxLength(200);
        });

        modelBuilder.Entity<BasCountry>(entity =>
        {
            entity.HasKey(e => e.CountryId);

            entity.Property(e => e.CountryCodeIso)
                .HasMaxLength(3)
                .HasColumnName("CountryCodeISO");
            entity.Property(e => e.CountryNameAr).HasMaxLength(200);
            entity.Property(e => e.CountryNameEn).HasMaxLength(200);
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NationalityNameFrn).HasMaxLength(200);
            entity.Property(e => e.NationalityNameNtv).HasMaxLength(200);
        });

        modelBuilder.Entity<DssFiscalYear>(entity =>
        {
            entity.HasNoKey();

            entity.Property(e => e.FiscalYear).HasMaxLength(10);
        });

        modelBuilder.Entity<DssHalfYearSetup>(entity =>
        {
            entity.HasKey(e => new { e.CustomerId, e.CmpNum, e.HalfYearCode });

            entity.ToTable("DssHalfYearSetup");

            entity.Property(e => e.CustomerId)
                .HasMaxLength(20)
                .IsFixedLength()
                .HasColumnName("CustomerID");
            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.HalfYearCode)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.HalfYearDescFrn)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.HalfYearDescNtv)
                .HasMaxLength(100)
                .IsFixedLength();
        });

        modelBuilder.Entity<DssHalfYearSetupDetail>(entity =>
        {
            entity.HasKey(e => new { e.CustomerId, e.CmpNum, e.HalfYearCode, e.PeriodNum });

            entity.Property(e => e.CustomerId)
                .HasMaxLength(20)
                .IsFixedLength()
                .HasColumnName("CustomerID");
            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.HalfYearCode)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.PeriodNum)
                .HasMaxLength(10)
                .IsFixedLength();
        });

        modelBuilder.Entity<DssPageStructure>(entity =>
        {
            entity.HasKey(e => new { e.CustomerId, e.CmpNum, e.PageId });

            entity.ToTable("DssPageStructure");

            entity.Property(e => e.CustomerId)
                .HasMaxLength(20)
                .IsUnicode(false);
            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.PageId)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.PageDataTable)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.PageDescFrn)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.PageDescNtv)
                .HasMaxLength(1000)
                .IsUnicode(false);
            entity.Property(e => e.Seg1)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg10)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg10LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg10LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg1LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg1LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg2)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg2LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg2LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg3)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg3LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg3LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg4)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg4LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg4LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg5)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg5LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg5LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg6)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg6LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg6LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg7)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg7LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg7LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg8)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg8LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg8LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg9)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg9LabelFrn)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Seg9LabelNtv)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<DssPeriodSetup>(entity =>
        {
            entity.HasKey(e => new { e.CmpNum, e.PeriodNum });

            entity.ToTable("DssPeriodSetup");

            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.PeriodNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.PeriodDescFrn)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.PeriodDescNtv)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.PeriodShortDesFrn)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.PeriodShortDesNtv)
                .HasMaxLength(10)
                .IsFixedLength();
        });

        modelBuilder.Entity<DssQuarterSetup>(entity =>
        {
            entity.HasKey(e => new { e.CustomerId, e.CmpNum, e.QuarterCode });

            entity.ToTable("DssQuarterSetup");

            entity.Property(e => e.CustomerId)
                .HasMaxLength(20)
                .IsFixedLength()
                .HasColumnName("CustomerID");
            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.QuarterCode)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.QuarterDescFrn)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.QuarterDescNtv)
                .HasMaxLength(100)
                .IsFixedLength();
        });

        modelBuilder.Entity<DssQuarterSetupDetail>(entity =>
        {
            entity.HasKey(e => new { e.CustomerId, e.CmpNum, e.QuarterCode, e.PeriodNum });

            entity.Property(e => e.CustomerId)
                .HasMaxLength(20)
                .IsFixedLength()
                .HasColumnName("CustomerID");
            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.QuarterCode)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.PeriodNum)
                .HasMaxLength(10)
                .IsFixedLength();
        });

        modelBuilder.Entity<DssSearchFileld>(entity =>
        {
            entity.HasKey(e => new { e.CustomerId, e.CmpNum, e.SearchField });

            entity.Property(e => e.CustomerId)
                .HasMaxLength(20)
                .IsFixedLength();
            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.SearchField)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.IconFile)
                .HasMaxLength(1000)
                .IsFixedLength();
            entity.Property(e => e.SearchFieldDescFrn)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.SearchFieldDescNtv)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.SearchFieldParam)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.SqlStmt).HasColumnType("ntext");
            entity.Property(e => e.TitleDescFrn)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.TitleDescNtv)
                .HasMaxLength(100)
                .IsFixedLength();
        });

        modelBuilder.Entity<DssSearchFileldsOption>(entity =>
        {
            entity.HasKey(e => new { e.CustomerId, e.CmpNum, e.SearchField, e.PageId });

            entity.ToTable("DssSearchFileldsOption");

            entity.Property(e => e.CustomerId)
                .HasMaxLength(20)
                .IsFixedLength();
            entity.Property(e => e.CmpNum)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.SearchField)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.PageId)
                .HasMaxLength(10)
                .IsFixedLength();
            entity.Property(e => e.PageOptionGroup)
                .HasMaxLength(100)
                .IsFixedLength();
        });

        modelBuilder.Entity<EntityProfile>(entity =>
        {
            entity.HasKey(e => e.EntityId);

            entity.ToTable("EntityProfile");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.EmailId).HasMaxLength(200);
            entity.Property(e => e.EntityNameAr).HasMaxLength(200);
            entity.Property(e => e.EntityNameEn).HasMaxLength(200);
            entity.Property(e => e.EntityType).HasMaxLength(20);
            entity.Property(e => e.GovernmentId)
                .HasMaxLength(20)
                .HasColumnName("GovernmentID");
            entity.Property(e => e.MobileNo).HasMaxLength(20);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.PhoneNo).HasMaxLength(20);
        });

        modelBuilder.Entity<FacilityProfile>(entity =>
        {
            entity.HasKey(e => e.FacilityId);

            entity.ToTable("FacilityProfile");

            entity.Property(e => e.City).HasMaxLength(100);
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.FacilityCode).HasMaxLength(200);
            entity.Property(e => e.FacilityNameAr).HasMaxLength(200);
            entity.Property(e => e.FacilityNameEn).HasMaxLength(200);
            entity.Property(e => e.Location).HasMaxLength(200);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.OragnizationName).HasMaxLength(200);
            entity.Property(e => e.Province).HasMaxLength(20);

            entity.HasOne(d => d.Entity).WithMany(p => p.FacilityProfiles).HasForeignKey(d => d.EntityId);
        });

        modelBuilder.Entity<InternalRole>(entity =>
        {
            entity.HasKey(e => e.RoleId);

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.RoleCode).HasMaxLength(200);
            entity.Property(e => e.RoleNameAr).HasMaxLength(200);
            entity.Property(e => e.RoleNameEn).HasMaxLength(200);
        });

        modelBuilder.Entity<InternalScreenRole>(entity =>
        {
            entity.HasKey(e => e.ScreenRoleId);

            entity.HasIndex(e => e.RoleId, "IX_InternalScreenRoles_RoleId");

            entity.HasIndex(e => e.ScreenId, "IX_InternalScreenRoles_ScreenId");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            entity.HasOne(d => d.Role).WithMany(p => p.InternalScreenRoles).HasForeignKey(d => d.RoleId);

            entity.HasOne(d => d.Screen).WithMany(p => p.InternalScreenRoles).HasForeignKey(d => d.ScreenId);
        });

        modelBuilder.Entity<ItemHierarchyStructure>(entity =>
        {
            entity.HasKey(e => e.ItemStructureCode);

            entity.ToTable("ItemHierarchyStructure");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ItemStructureDesFrn).HasMaxLength(120);
            entity.Property(e => e.ItemStructureDesNtv).HasMaxLength(120);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
        });

        modelBuilder.Entity<ItemSourcesFile>(entity =>
        {
            entity.Property(e => e.ContentType).HasMaxLength(200);
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.FileName).HasMaxLength(300);
            entity.Property(e => e.FileOriginalName).HasMaxLength(300);
            entity.Property(e => e.FilePath).HasMaxLength(4000);
            entity.Property(e => e.FileSourceId).HasColumnName("FileSourceID");
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
        });

        modelBuilder.Entity<ItemSourcesProfile>(entity =>
        {
            entity.HasKey(e => e.SourceId);

            entity.ToTable("ItemSourcesProfile");

            entity.Property(e => e.AssociatedEquipment).HasMaxLength(30);
            entity.Property(e => e.ChemicalComposition).HasMaxLength(30);
            entity.Property(e => e.ChemicalForm).HasMaxLength(30);
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.CurrentActivity).HasMaxLength(30);
            entity.Property(e => e.CurrentActivityUnit).HasMaxLength(30);
            entity.Property(e => e.EnrichmentofU235).HasMaxLength(30);
            entity.Property(e => e.Exemption).HasMaxLength(30);
            entity.Property(e => e.FacilitySerialNo).HasMaxLength(20);
            entity.Property(e => e.InitialActivity).HasMaxLength(30);
            entity.Property(e => e.InitialActivityUnit).HasMaxLength(30);
            entity.Property(e => e.IspotopHalfLifeTimeUnit).HasMaxLength(30);
            entity.Property(e => e.ManufacturerSerialNo).HasMaxLength(30);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NrrcId)
                .HasMaxLength(20)
                .HasColumnName("NrrcID");
            entity.Property(e => e.NuclearMaterial).HasMaxLength(30);
            entity.Property(e => e.PhysicalForm).HasMaxLength(30);
            entity.Property(e => e.Purpose).HasMaxLength(300);
            entity.Property(e => e.RadioNuclides).HasMaxLength(30);
            entity.Property(e => e.RadioactiveSourceid).HasMaxLength(30);
            entity.Property(e => e.ShieldMaterial).HasMaxLength(30);
            entity.Property(e => e.ShieldNuclearMaterialCode).HasMaxLength(30);
            entity.Property(e => e.SourceActivity).HasMaxLength(30);
            entity.Property(e => e.SourceCategory).HasMaxLength(30);
            entity.Property(e => e.SourceDescAr).HasMaxLength(200);
            entity.Property(e => e.SourceDescEn).HasMaxLength(200);
            entity.Property(e => e.SourceModel).HasMaxLength(30);
            entity.Property(e => e.Sroid).HasColumnName("SROId");
            entity.Property(e => e.Status).HasMaxLength(30);
            entity.Property(e => e.Unit).HasMaxLength(30);
            entity.Property(e => e.WeightOffIssileisotopes).HasMaxLength(30);
            entity.Property(e => e.WeightOffIssileisotopesunit).HasMaxLength(30);

            entity.HasOne(d => d.Entity).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.EntityId);

            entity.HasOne(d => d.Facility).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.FacilityId);

            entity.HasOne(d => d.ItemType).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.ItemTypeId);

            entity.HasOne(d => d.LegalRepresentatives).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.LegalRepresentativesId);

            entity.HasOne(d => d.License).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.LicenseId);

            entity.HasOne(d => d.LicenseInventory).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.LicenseInventoryId);

            entity.HasOne(d => d.ManufactureCountry).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.ManufactureCountryId);

            entity.HasOne(d => d.Manufacturer).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.ManufacturerId);

            entity.HasOne(d => d.PermitInventory).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.PermitInventoryId);

            entity.HasOne(d => d.Permitdetails).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.PermitdetailsId);

            entity.HasOne(d => d.Practise).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.PractiseId);

            entity.HasOne(d => d.Sro).WithMany(p => p.ItemSourcesProfiles).HasForeignKey(d => d.Sroid);
        });

        modelBuilder.Entity<ItemSourcesRadionulcide>(entity =>
        {
            entity.HasIndex(e => e.ItemSourceProfileId, "IX_ItemSourcesRadionulcides_ItemSourceProfileId");

            entity.HasIndex(e => e.RadionulcideId, "IX_ItemSourcesRadionulcides_RadionulcideId");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.InitialActivity).HasMaxLength(100);
            entity.Property(e => e.InitialActivityUnit).HasMaxLength(100);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            entity.HasOne(d => d.ItemSourceProfile).WithMany(p => p.ItemSourcesRadionulcides).HasForeignKey(d => d.ItemSourceProfileId);

            entity.HasOne(d => d.Radionulcide).WithMany(p => p.ItemSourcesRadionulcides).HasForeignKey(d => d.RadionulcideId);
        });

        modelBuilder.Entity<LegalRepresentativesProfile>(entity =>
        {
            entity.HasKey(e => e.LegalRepresentativesId);

            entity.ToTable("LegalRepresentativesProfile");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.EmailId).HasMaxLength(200);
            entity.Property(e => e.LegalRepresentativesNameAr).HasMaxLength(200);
            entity.Property(e => e.LegalRepresentativesNameEn).HasMaxLength(200);
            entity.Property(e => e.MobileNo).HasMaxLength(20);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NationalId)
                .HasMaxLength(20)
                .HasColumnName("NationalID");
            entity.Property(e => e.Note).HasMaxLength(4000);
            entity.Property(e => e.PhoneNo).HasMaxLength(20);
            entity.Property(e => e.Status).HasMaxLength(30);
            entity.Property(e => e.Title).HasMaxLength(200);
        });

        modelBuilder.Entity<LicenseInventoryLimit>(entity =>
        {
            entity.HasKey(e => e.LicenseInventoryId);

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            entity.HasOne(d => d.License).WithMany(p => p.LicenseInventoryLimits).HasForeignKey(d => d.LicenseId);
        });

        modelBuilder.Entity<LicenseProfile>(entity =>
        {
            entity.HasKey(e => e.LicenseId);

            entity.ToTable("LicenseProfile");

            entity.Property(e => e.LicenseActivities).HasMaxLength(4000);
            entity.Property(e => e.LicenseCode).HasMaxLength(30);
            entity.Property(e => e.LicenseDescAr).HasMaxLength(200);
            entity.Property(e => e.LicenseDescEn).HasMaxLength(200);
            entity.Property(e => e.LicenseVersionNumber).HasMaxLength(20);
            entity.Property(e => e.PractiseSector).HasMaxLength(20);

            entity.HasOne(d => d.Entity).WithMany(p => p.LicenseProfiles).HasForeignKey(d => d.EntityId);

            entity.HasOne(d => d.Facility).WithMany(p => p.LicenseProfiles).HasForeignKey(d => d.FacilityId);
        });

        modelBuilder.Entity<ListOfValue>(entity =>
        {
            entity.HasKey(e => e.LovId);

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.LovCode).HasMaxLength(200);
            entity.Property(e => e.LovNameAr).HasMaxLength(200);
            entity.Property(e => e.LovNameEn).HasMaxLength(200);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.SqlStatement).HasMaxLength(2000);
        });

        modelBuilder.Entity<LookupSet>(entity =>
        {
            entity.ToTable("LookupSet");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
        });

        modelBuilder.Entity<LookupSetTerm>(entity =>
        {
            entity.ToTable("LookupSetTerm");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            entity.HasOne(d => d.LookupSet).WithMany(p => p.LookupSetTerms)
                .HasForeignKey(d => d.LookupSetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LookupSetTerm_LookupSet");
        });

        modelBuilder.Entity<ManufacturerMaster>(entity =>
        {
            entity.HasKey(e => e.ManufacturerId);

            entity.ToTable("ManufacturerMaster");

            entity.Property(e => e.AddressLine1).HasMaxLength(200);
            entity.Property(e => e.AddressLine2).HasMaxLength(200);
            entity.Property(e => e.AddressLine3).HasMaxLength(200);
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.EmailId).HasMaxLength(200);
            entity.Property(e => e.Location).HasMaxLength(200);
            entity.Property(e => e.ManufacturerDescAr).HasMaxLength(200);
            entity.Property(e => e.ManufacturerDescEn).HasMaxLength(200);
            entity.Property(e => e.MobileNo).HasMaxLength(20);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.PhoneNo).HasMaxLength(20);
            entity.Property(e => e.Pobox)
                .HasMaxLength(20)
                .HasColumnName("POBox");
            entity.Property(e => e.ZipCode).HasMaxLength(20);

            entity.HasOne(d => d.Country).WithMany(p => p.ManufacturerMasters).HasForeignKey(d => d.CountryId);
        });

        modelBuilder.Entity<NuclearRelatedItemsProfile>(entity =>
        {
            entity.HasKey(e => e.NrrcrelatedItemId);

            entity.ToTable("NuclearRelatedItemsProfile");

            entity.Property(e => e.NrrcrelatedItemId).HasColumnName("NRRCRelatedItemId");
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.EndUserCertificateFlag).HasMaxLength(30);
            entity.Property(e => e.FacilityRelatedItemId)
                .HasMaxLength(30)
                .HasColumnName("FacilityRelatedItemID");
            entity.Property(e => e.GovernmentCommitmentsFlag).HasMaxLength(30);
            entity.Property(e => e.Hscode)
                .HasMaxLength(30)
                .HasColumnName("HSCode");
            entity.Property(e => e.ItemTypeNo).HasMaxLength(30);
            entity.Property(e => e.ItemtypeName).HasMaxLength(30);
            entity.Property(e => e.ManufacturerSerialNo).HasMaxLength(30);
            entity.Property(e => e.ModelNumber).HasMaxLength(30);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NrrcId)
                .HasMaxLength(20)
                .HasColumnName("NrrcID");
            entity.Property(e => e.Purpose).HasMaxLength(30);
            entity.Property(e => e.RelatedItemDescAr).HasMaxLength(200);
            entity.Property(e => e.RelatedItemDescEn).HasMaxLength(200);
            entity.Property(e => e.Sroid).HasColumnName("SROId");
            entity.Property(e => e.Status).HasMaxLength(30);
            entity.Property(e => e.Unit).HasMaxLength(30);

            entity.HasOne(d => d.Entity).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.EntityId);

            entity.HasOne(d => d.Facility).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.FacilityId);

            entity.HasOne(d => d.ItemCategoryNavigation).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.ItemCategory);

            entity.HasOne(d => d.LegalRepresentatives).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.LegalRepresentativesId);

            entity.HasOne(d => d.License).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.LicenseId);

            entity.HasOne(d => d.LicenseInventory).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.LicenseInventoryId);

            entity.HasOne(d => d.ManufactureCountry).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.ManufactureCountryId);

            entity.HasOne(d => d.Manufacturer).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.ManufacturerId);

            entity.HasOne(d => d.PermitInventory).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.PermitInventoryId);

            entity.HasOne(d => d.Permitdetails).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.PermitdetailsId);

            entity.HasOne(d => d.Practise).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.PractiseId);

            entity.HasOne(d => d.Sro).WithMany(p => p.NuclearRelatedItemsProfiles).HasForeignKey(d => d.Sroid);
        });

        modelBuilder.Entity<PermitDetailsProfile>(entity =>
        {
            entity.HasKey(e => e.PermitDetailsId);

            entity.ToTable("PermitDetailsProfile");

            entity.Property(e => e.PermitDetailsDescAr).HasMaxLength(200);
            entity.Property(e => e.PermitDetailsDescEn).HasMaxLength(200);
            entity.Property(e => e.PermitNumber).HasMaxLength(200);

            entity.HasOne(d => d.License).WithMany(p => p.PermitDetailsProfiles).HasForeignKey(d => d.LicenseId);
        });

        modelBuilder.Entity<PermitInventoryLimit>(entity =>
        {
            entity.HasKey(e => e.PermitInventoryId);

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ManufactureName).HasMaxLength(200);
            entity.Property(e => e.ModelMaximumRadioactivity).HasMaxLength(30);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.Radionuclide).HasMaxLength(30);
            entity.Property(e => e.SourceSerialNo).HasMaxLength(30);
            entity.Property(e => e.Unit).HasMaxLength(30);

            entity.HasOne(d => d.PermitDetails).WithMany(p => p.PermitInventoryLimits).HasForeignKey(d => d.PermitDetailsId);
        });

        modelBuilder.Entity<PractiseProfile>(entity =>
        {
            entity.HasKey(e => e.PractiseId);

            entity.ToTable("PractiseProfile");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.PractiseNameAr).HasMaxLength(200);
            entity.Property(e => e.PractiseNameEn).HasMaxLength(200);

            entity.HasOne(d => d.PermitDetails).WithMany(p => p.PractiseProfiles).HasForeignKey(d => d.PermitDetailsId);
        });

        modelBuilder.Entity<RadiationGeneratorsProfile>(entity =>
        {
            entity.HasKey(e => e.EquipmentId);

            entity.ToTable("RadiationGeneratorsProfile");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.DoseUnit).HasMaxLength(30);
            entity.Property(e => e.EnergyUnit).HasMaxLength(30);
            entity.Property(e => e.EquipmentDescAr).HasMaxLength(200);
            entity.Property(e => e.EquipmentDescEn).HasMaxLength(200);
            entity.Property(e => e.FacilitySerialNo).HasMaxLength(20);
            entity.Property(e => e.ManufacturerSerialNo).HasMaxLength(20);
            entity.Property(e => e.MaxCurrent).HasMaxLength(30);
            entity.Property(e => e.MaxDoseRate).HasMaxLength(30);
            entity.Property(e => e.MaxEnergy).HasMaxLength(30);
            entity.Property(e => e.ModelNumber).HasMaxLength(30);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NrrcId)
                .HasMaxLength(20)
                .HasColumnName("NrrcID");
            entity.Property(e => e.Purpose).HasMaxLength(30);
            entity.Property(e => e.SheildMaterial).HasMaxLength(30);
            entity.Property(e => e.ShieldNuclearMaterialCode).HasMaxLength(30);
            entity.Property(e => e.Sroid).HasColumnName("SROId");
            entity.Property(e => e.Status).HasMaxLength(30);
            entity.Property(e => e.Unit).HasMaxLength(30);
            entity.Property(e => e.XrayTubeSerialNo)
                .HasMaxLength(30)
                .HasColumnName("XRayTubeSerialNo");

            entity.HasOne(d => d.Entity).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.EntityId);

            entity.HasOne(d => d.EquipmentTypeNavigation).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.EquipmentType);

            entity.HasOne(d => d.Facility).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.FacilityId);

            entity.HasOne(d => d.LegalRepresentatives).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.LegalRepresentativesId);

            entity.HasOne(d => d.License).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.LicenseId);

            entity.HasOne(d => d.LicenseInventory).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.LicenseInventoryId);

            entity.HasOne(d => d.Manufacturer).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.ManufacturerId);

            entity.HasOne(d => d.PermitInventory).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.PermitInventoryId);

            entity.HasOne(d => d.Permitdetails).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.PermitdetailsId);

            entity.HasOne(d => d.Practise).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.PractiseId);

            entity.HasOne(d => d.Sro).WithMany(p => p.RadiationGeneratorsProfiles).HasForeignKey(d => d.Sroid);
        });

        modelBuilder.Entity<Radionuclide>(entity =>
        {
            entity.Property(e => e.ActivityUnit)
                .HasMaxLength(200)
                .HasDefaultValueSql("(N'')");
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.DisplayName)
                .HasMaxLength(75)
                .HasDefaultValueSql("(N'')");
            entity.Property(e => e.Dvalue).HasColumnName("DValue");
            entity.Property(e => e.DvalueUnit)
                .HasMaxLength(10)
                .HasDefaultValueSql("(N'')")
                .HasColumnName("DValueUnit");
            entity.Property(e => e.ExemptionValueUnit)
                .HasMaxLength(10)
                .HasDefaultValueSql("(N'')");
            entity.Property(e => e.HalfLifeUnit).HasMaxLength(10);
            entity.Property(e => e.IsActive)
                .IsRequired()
                .HasDefaultValueSql("(CONVERT([bit],(0)))");
            entity.Property(e => e.Isotop).HasMaxLength(200);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
        });

        modelBuilder.Entity<SafetyResponsibleOfficersProfile>(entity =>
        {
            entity.HasKey(e => e.Sroid);

            entity.ToTable("SafetyResponsibleOfficersProfile");

            entity.Property(e => e.Sroid).HasColumnName("SROId");
            entity.Property(e => e.CertificateNo).HasMaxLength(30);
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.EmailId).HasMaxLength(200);
            entity.Property(e => e.MobileNo).HasMaxLength(20);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NationalId)
                .HasMaxLength(20)
                .HasColumnName("NationalID");
            entity.Property(e => e.PhoneNo).HasMaxLength(20);
            entity.Property(e => e.SronameAr)
                .HasMaxLength(200)
                .HasColumnName("SRONameAr");
            entity.Property(e => e.SronameEn)
                .HasMaxLength(200)
                .HasColumnName("SRONameEn");
        });

        modelBuilder.Entity<Screen>(entity =>
        {
            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.ScreenCode).HasMaxLength(200);
            entity.Property(e => e.ScreenNameAr).HasMaxLength(200);
            entity.Property(e => e.ScreenNameEn).HasMaxLength(200);
        });

        modelBuilder.Entity<ScreenField>(entity =>
        {
            entity.HasKey(e => e.FieldId);

            entity.HasIndex(e => e.LookupSetId, "IX_ScreenFields_LookupSetId");

            entity.HasIndex(e => e.LovId, "IX_ScreenFields_LovId");

            entity.HasIndex(e => e.ScreenId, "IX_ScreenFields_ScreenId");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.FieldDescAr).HasMaxLength(200);
            entity.Property(e => e.FieldDescEn).HasMaxLength(200);
            entity.Property(e => e.FieldFormat).HasMaxLength(200);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            entity.HasOne(d => d.LookupSet).WithMany(p => p.ScreenFields).HasForeignKey(d => d.LookupSetId);

            entity.HasOne(d => d.Lov).WithMany(p => p.ScreenFields).HasForeignKey(d => d.LovId);

            entity.HasOne(d => d.Screen).WithMany(p => p.ScreenFields).HasForeignKey(d => d.ScreenId);
        });

        modelBuilder.Entity<ServiceItemPrice>(entity =>
        {
            entity.ToTable("ServiceItemPrice");

            entity.HasIndex(e => e.ServiceItemId, "IX_ServiceItemPrice_ServiceItemId");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.ItemPrice).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            entity.HasOne(d => d.ServiceItem).WithMany(p => p.ServiceItemPrices).HasForeignKey(d => d.ServiceItemId);
        });

        modelBuilder.Entity<ServiceItemProfile>(entity =>
        {
            entity.HasKey(e => e.ServiceItemId);

            entity.ToTable("ServiceItemProfile");

            entity.HasIndex(e => e.ItemStructureCode1, "IX_ServiceItemProfile_ItemStructureCode1");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.CurrentPrice).HasColumnType("decimal(18, 2)");
            entity.Property(e => e.ItemDesFrn).HasMaxLength(200);
            entity.Property(e => e.ItemDesNtv).HasMaxLength(200);
            entity.Property(e => e.ItemStructureCode).HasMaxLength(200);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);

            entity.HasOne(d => d.ItemStructureCode1Navigation).WithMany(p => p.ServiceItemProfiles).HasForeignKey(d => d.ItemStructureCode1);
        });

        modelBuilder.Entity<SourceTypeMaster>(entity =>
        {
            entity.HasKey(e => e.ItemTypeId);

            entity.ToTable("SourceTypeMaster");

            entity.Property(e => e.ItemTypeId).HasColumnName("ItemTypeID");
            entity.Property(e => e.CreatedUser).HasMaxLength(30);
            entity.Property(e => e.ItemTypeClass).HasMaxLength(30);
            entity.Property(e => e.ItemTypeDescFrn).HasMaxLength(200);
            entity.Property(e => e.ItemTypeDescNtv).HasMaxLength(200);
            entity.Property(e => e.ModifiedUser).HasMaxLength(30);
        });

        modelBuilder.Entity<TreeControl>(entity =>
        {
            entity.HasKey(e => new { e.TreeCode, e.LevelNum });

            entity.ToTable("TreeControl");

            entity.Property(e => e.LevelPadding).HasMaxLength(20);
            entity.Property(e => e.LevelTitleFrn)
                .HasMaxLength(100)
                .HasColumnName("LevelTitleFRN");
            entity.Property(e => e.LevelTitleNtv)
                .HasMaxLength(100)
                .HasColumnName("LevelTitleNTV");
        });

        modelBuilder.Entity<Worker>(entity =>
        {
            entity.HasIndex(e => e.FacilityId, "IX_Workers_FacilityId");

            entity.HasIndex(e => e.LookupSetId, "IX_Workers_LookupSetId");

            entity.Property(e => e.CreatedBy).HasMaxLength(50);
            entity.Property(e => e.JobPosition).HasMaxLength(200);
            entity.Property(e => e.ModifiedBy).HasMaxLength(50);
            entity.Property(e => e.NationalityId).HasMaxLength(200);
            entity.Property(e => e.PassportNo).HasMaxLength(200);
            entity.Property(e => e.WorkerNameAr).HasMaxLength(200);
            entity.Property(e => e.WorkerNameEn).HasMaxLength(200);

            entity.HasOne(d => d.Facility).WithMany(p => p.Workers).HasForeignKey(d => d.FacilityId);

            entity.HasOne(d => d.LookupSet).WithMany(p => p.Workers).HasForeignKey(d => d.LookupSetId);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
