﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class ItemSourcesFile
{
    public int ItemSourcesFileId { get; set; }

    public int FileSourceId { get; set; }

    public int FileNum { get; set; }

    public string? FileName { get; set; }

    public string? FileOriginalName { get; set; }

    public int FileSize { get; set; }

    public int UploadType { get; set; }

    public string? FilePath { get; set; }

    public byte[]? FileBytes { get; set; }

    public string? ContentType { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }
}
