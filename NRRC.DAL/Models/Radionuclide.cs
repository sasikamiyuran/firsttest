﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class Radionuclide
{
    public int RadionuclideId { get; set; }

    public string Isotop { get; set; } = null!;

    public float Dvalue { get; set; }

    public float HalfLife { get; set; }

    public string HalfLifeUnit { get; set; } = null!;

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public string ActivityUnit { get; set; } = null!;

    public string DisplayName { get; set; } = null!;

    public float ExemptionValue { get; set; }

    public string ExemptionValueUnit { get; set; } = null!;

    public float InitialActivity { get; set; }

    public bool? IsActive { get; set; }

    public string DvalueUnit { get; set; } = null!;

    public virtual ICollection<ItemSourcesRadionulcide> ItemSourcesRadionulcides { get; } = new List<ItemSourcesRadionulcide>();
}
