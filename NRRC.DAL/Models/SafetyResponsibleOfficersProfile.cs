﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class SafetyResponsibleOfficersProfile
{
    public int Sroid { get; set; }

    public string? SronameAr { get; set; }

    public string? SronameEn { get; set; }

    public string? NationalId { get; set; }

    public string? PhoneNo { get; set; }

    public string? MobileNo { get; set; }

    public string? EmailId { get; set; }

    public string? CertificateNo { get; set; }

    public DateTime IssuanceDate { get; set; }

    public DateTime ExpiryDate { get; set; }

    public DateTime? AmanInsertedOn { get; set; }

    public string? CreatedBy { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? CreatedOn { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<ItemSourcesProfile> ItemSourcesProfiles { get; } = new List<ItemSourcesProfile>();

    public virtual ICollection<NuclearRelatedItemsProfile> NuclearRelatedItemsProfiles { get; } = new List<NuclearRelatedItemsProfile>();

    public virtual ICollection<RadiationGeneratorsProfile> RadiationGeneratorsProfiles { get; } = new List<RadiationGeneratorsProfile>();
}
