﻿using System;
using System.Collections.Generic;

namespace NRRC.DAL.Models;

public partial class DssSearchFileldsOption
{
    public string CustomerId { get; set; } = null!;

    public string CmpNum { get; set; } = null!;

    public string SearchField { get; set; } = null!;

    public string PageId { get; set; } = null!;

    public string? PageOptionGroup { get; set; }

    public double? PageOptionGroupOrder { get; set; }

    public int? ShowHideFlag { get; set; }
}
