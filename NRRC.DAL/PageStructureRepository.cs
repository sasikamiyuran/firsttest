﻿using Microsoft.EntityFrameworkCore;
using NRRC.DAL.Context;
using NRRC.DAL.Repositories.IRepositories;
using NRRC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GE = NRRC.Entity;

namespace NRRC.DAL
{
    public class PageStructureRepository: IPageStructureRepository
    {
        private readonly NrrcdbContext _context;
        public PageStructureRepository(NrrcdbContext context)
        {
            this._context = context;
        }



        //GE::PageStructure GetPageStructures(string customerId, string companyNumber, string pageId)
        //{
        //    var data = this._context.DssPageStructures.Include(page => page.PageId == pageId).FirstOrDefault();
        //    PageStructure pageStructure = new PageStructure();
        //    if (data != null)
        //    {
        //        pageStructure.PageId = data.PageId;
        //        pageStructure.CustomerId = data.CustomerId;
        //        pageStructure.CmpNum = data.CmpNum;
        //        pageStructure.Seg1LabelFrn = data.Seg1LabelFrn;
        //    }
        //    return pageStructure;
        //}

        PageStructure IPageStructureRepository.GetPageStructures(string customerId, string companyNumber, string pageId)
        {
            var data = this._context.DssPageStructures.Include(page => page.PageId == pageId).FirstOrDefault();
            PageStructure pageStructure = new PageStructure();
            if (data != null)
            {
                pageStructure.PageId = data.PageId;
                pageStructure.CustomerId = data.CustomerId;
                pageStructure.CmpNum = data.CmpNum;
                pageStructure.Seg1LabelFrn = data.Seg1LabelFrn;
            }
            return pageStructure;
        }
    }
}
